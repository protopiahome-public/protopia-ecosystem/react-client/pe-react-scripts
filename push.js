const simpleGit = require('simple-git');
const path = require('path');

const paths = [];

paths.push(path.resolve(`${__dirname}/../`));
paths.push(path.resolve(`${__dirname}/../src/layouts`));
paths.push(path.resolve(`${__dirname}/../src/LayoutApp`));
paths.push(path.resolve(`${__dirname}/../src/config`));

const widgets = require('../src/config/widgets.json');

if (widgets) {
    widgets.forEach((e, i) => {
        if (e.name) {
            paths.push(path.resolve(`${__dirname}/../src/widgets/${e.name}`));
        }
    });
}

const views = require('../src/config/views.json');

if (views) {
    views.forEach((e, i) => {
        if (e.name) {
            paths.push(path.resolve(`${__dirname}/../src/states/${e.name}`));
        }
    });
}

if (!process.argv[2]) {
    console.log('Write message');
    process.exit();
}

const message = process.argv.slice(2).join(' ');

const gitRun = async () => {
    for (const k in paths) {
        const git = simpleGit({ baseDir: paths[k] });
        const changes = await git.status();
        if (changes.files.length) {
            console.log(`${paths[k]} started...`);
            await git.raw('add', '-A');
            console.log(`${paths[k]} added`);
            await git.commit(message);
            console.log(`${paths[k]} commited`);
            await git.push();
            console.log(`${paths[k]} pushed.`);
        } else {
            console.log(`${paths[k]} only push started...`);
            await git.push();
            console.log(`${paths[k]} pushed.`);
        }
    }
};
gitRun();
