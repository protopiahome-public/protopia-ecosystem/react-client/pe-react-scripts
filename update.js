// TODO доработать до 0.5
import modules from '../src/config/widgets.json';

const fs = require('fs');
const child_process = require('child_process');

function update(directory) {
    directory = `${__dirname}/../src/${directory}`;
    if (fs.existsSync(directory)) {
        child_process.execSync(`cd ${directory} && git pull`);
    }
}

function updateWidget(directory) {
    directory = `${__dirname}/../src/widgets/${directory}`;
    if (fs.existsSync(directory)) {
        child_process.execSync(`cd ${directory} && git pull`);
    }
}

function updateViews(directory) {
    directory = `${__dirname}/../src/states/${directory}`;
    if (fs.existsSync(directory)) {
        child_process.execSync(`cd ${directory} && git pull`);
    }
}

update('.');
update('layouts');
update('LayoutApp');
update('config');

const widgets = require('../src/config/widgets.json');

if (widgets) {
    widgets.forEach((e, i) => {
        if (e.name) {
            updateWidget(e.name);
        }
    });
}

const views = require('../src/config/views.json');

if (views) {
    views.forEach((e, i) => {
        if (e.name) {
            updateViews(e.name);
        }
    });
}
