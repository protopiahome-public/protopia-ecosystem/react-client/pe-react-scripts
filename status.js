const simpleGit = require('simple-git');
const path = require('path');

const paths = [];

paths.push(path.resolve(`${__dirname}/../`));
paths.push(path.resolve(`${__dirname}/../src/config`));
paths.push(path.resolve(`${__dirname}/../src/layouts`));
paths.push(path.resolve(`${__dirname}/../src/LayoutApp`));

const widgets = require('../src/config/widgets.json');

if (widgets) {
    widgets.forEach((e, i) => {
        if (e.name) {
            paths.push(path.resolve(`${__dirname}/../src/widgets/${e.name}`));
        }
    });
}

const views = require('../src/config/views.json');

if (views) {
    views.forEach((e, i) => {
        if (e.name) {
            paths.push(path.resolve(`${__dirname}/../src/states/${e.name}`));
        }
    });
}

const gitRun = async () => {
    for (const k in paths) {
        const git = simpleGit({ baseDir: paths[k] });
        const changes = await git.status();
        if (changes.files.length) {
            console.log(`${paths[k]}:`);
            changes.files.forEach(file => console.log(file.path));
            console.log('');
        }
    }
};
gitRun();
